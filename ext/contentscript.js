// The background page is asking us to find an address on the page.
chrome.extension.onRequest.addListener(function (sentMsg, sender, callback) {

    if (sentMsg.log) {
        console.log(sentMsg.log);
    }

    else if (sentMsg.setEnabled) {
        localStorage.setItem('NikeFormFill', sentMsg.isEnabled);
        console.log('setEnabled: ' + localStorage.getItem('NikeFormFill'))

    }

    else if (sentMsg.checkVisibility) {
        callback(location.href.match(/nike/gi));
    }

    else if (sentMsg.getIsEnabled) {
        var isEnabled;
        if (localStorage.getItem('NikeFormFill') === "true") {
            isEnabled = true;
        } else if (localStorage.getItem('NikeFormFill') === "false") {
            isEnabled = false;
        } else { //if the localStorage is not yet set, set it to isEnabled.
            isEnabled = true;
        }
        callback(isEnabled);
    }

    else if (sentMsg.doFormFill) {
        //catch fail since jsonp fails silently.
        var catchFail = setTimeout(function () {
            //throw Error("lame... auto-fill bookmarklet failed to load.");
        }, 3000)
        var script = document.createElement('script');
        script.type = 'text/javascript';
        script.async = true;
        script.charset = 'UTF-8';
        script.setAttribute('src', 'https://bitbucket.org/ocp-tools/ext/raw/master/AutoFillbookmarklet.js#' + (new Date().getTime()).toString().slice(0, 6));//Shave off to 1,000,000th place to cache it 2.78 hrs )
        document.body.appendChild(script);
        script.onload = script.onreadystatechange = function () {
            var rs = script.readyState;
            if (!rs || rs === 'loaded' || rs === 'complete') {
                clearTimeout(catchFail);
                script.onload = script.onreadystatechange = null;
            }
        };
    }

});
 