var enabledForTabs = true;
var clicks = 0; //same count for all tabs

// get/set visibility
var getIconVisibilityResult;
function getIconVisibility(tabId) {
    chrome.tabs.sendRequest(tabId, { checkVisibility: true }, function (found) {
        getIconVisibilityResult = found;
    });
    return getIconVisibilityResult;
}
function setIconVisibility(tabId, isVisible) {
    if (isVisible) {
        chrome.pageAction.show(tabId);
    } else {
        chrome.pageAction.hide(tabId);
    }
}

function setEnabled(tabId, isEnabled) {
    if (isEnabled) {
        chrome.pageAction.setIcon({ imageData: draw('coral'), tabId: tabId });
        enabledForTabs = true;
        chrome.pageAction.setTitle({ tabId: tabId, title: "Turned ON for all tabs." });
    } else {
        chrome.pageAction.setIcon({ imageData: draw('lightgray'), tabId: tabId });
        enabledForTabs = false;
        chrome.pageAction.setTitle({ tabId: tabId, title: "Turned OFF for all tabs." });
    }
}

//on initial load of page, query all tab, filter to set parameters
chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
    doFormFill(tabs[0].id);
});

//on page reload
chrome.tabs.onUpdated.addListener(function (tabId, change, tab) {
    if (change.status == "complete") {
        doFormFill(tabId);
    }
});

//on tab change
chrome.tabs.onSelectionChanged.addListener(function (tabId) {
    doFormFill(tabId);
});

//icon click
var tab_clicks = {};
chrome.pageAction.onClicked.addListener(function (tab) {
    chrome.tabs.sendRequest(tab.id, { log: clicks }, function () { });
    clicks++;
    if (clicks % 2) {
        setEnabled(tab.id, false);
        enabledForTabs = false;
    } else {
        setEnabled(tab.id, true);
        enabledForTabs = true;
        chrome.tabs.sendRequest(tab.id, { doFormFill: true }, function () { });
    }
});

function doFormFill(tabId) {

    //Chrome has treading delay issues, it's best to repeat 4x
    var repeats = 0;
    var setIconInterval = setInterval(function () {

        var isVisible = getIconVisibility(tabId);
        if (isVisible) { //if Nike, show

            setEnabled(tabId, enabledForTabs);
            setIconVisibility(tabId, true);

            if (enabledForTabs && repeats === 1) {//Call doFormFill once.
                chrome.tabs.sendRequest(tabId, { doFormFill: true }, function () { });
            }
        } else {
            setIconVisibility(tabId, false);
        }

        repeats++
        if (repeats > 4) {
            clearInterval(setIconInterval);
        }
    }, 250);

}

/// Canvas Icon 
function draw(color) {
    var canvas = document.getElementById('canvas');
    var ctx = canvas.getContext("2d");

    var xoff = 85,
        yoff = 5
    scale = 0.067;
    // Store the current transformation matrix
    ctx.save();

    // Use the identity matrix while clearing the canvas
    ctx.setTransform(1, 0, 0, 1, 0, 0);
    ctx.clearRect(0, 0, canvas.width, canvas.height);

    ctx.restore();

    ctx.lineWidth = 0.67;
    ctx.strokeStyle = (color === 'undefined') ? 'coral' : color;

    ctx.beginPath();
    ctx.moveTo(scale * (136 - xoff), scale * (122 - yoff));
    ctx.bezierCurveTo(scale * (87 - xoff), scale * (160 - yoff), scale * (55 - xoff), scale * (273 - yoff), scale * (181 - xoff), scale * (220 - yoff));
    ctx.stroke();

    ctx.beginPath();
    ctx.moveTo(scale * (136 - xoff), scale * (122 - yoff));
    ctx.bezierCurveTo(scale * (126 - xoff), scale * (143 - yoff), scale * (101 - xoff), scale * (210 - yoff), scale * (205 - xoff), scale * (177 - yoff));
    //ctx.strokeStyle = 'red';
    ctx.stroke();

    ctx.beginPath();
    ctx.moveTo(scale * (205 - xoff), scale * (177 - yoff));
    ctx.lineTo(scale * (419 - xoff), scale * (120 - yoff));
    //ctx.strokeStyle = 'green';
    ctx.stroke();

    ctx.beginPath();
    ctx.moveTo(scale * (181 - xoff), scale * (220 - yoff));
    ctx.lineTo(scale * (419 - xoff), scale * (120 - yoff));
    //ctx.strokeStyle = 'blue';
    ctx.stroke();

    return ctx.getImageData(0, 0, 19, 19);
}
